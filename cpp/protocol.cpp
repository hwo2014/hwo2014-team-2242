#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json bot_id;
    bot_id["name"] = name;
    bot_id["key"] = key;

    jsoncons::json data;
    data["botId"] = bot_id;
    data["trackName"] = "germany";
    data["carCount"] = 1;

    return make_request("joinRace", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

}  // namespace hwo_protocol
