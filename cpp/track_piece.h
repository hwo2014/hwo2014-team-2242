#include <sstream>
#include <string>

class TrackPiece {
protected:
  bool is_bent_;

public:
  virtual std::string ToString() const = 0;

  bool is_bent() const {
    return is_bent_;
  }

  virtual ~TrackPiece() {}
};

class StraightTrackPiece : public TrackPiece {
public:
  StraightTrackPiece(double l)
    : length_(l) {
    is_bent_ = false;
  }

  double length() const {
    return length_;
  }

  virtual std::string ToString() const {
    std::stringstream out;
    out << "**Straight-- length= " << length() << std::endl;

    return out.str();
  }

  virtual ~StraightTrackPiece() {}

private:
  double length_;
};

class BentTrackPiece : public TrackPiece {
public:
  BentTrackPiece(double r, double a)
    : radius_(r), angle_(a) {
    is_bent_ = true;
  }

  double radius() const {
    return radius_;
  }

  double angle() const {
    return angle_;
  }

  virtual std::string ToString() const {
    std::stringstream out;
    out << "** Bent Piece-- radius= " << radius() << ", angle= " << angle() << std::endl;

    return out.str();
  }

  virtual ~BentTrackPiece() {}

private:
  double radius_;
  double angle_;
};
