#include "game_logic.h"
#include "protocol.h"

#include "math.h"
#include "assert.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {
  const jsoncons::json& track = data["race"]["track"]["pieces"];

  for (size_t i = 0; i < track.size(); ++i) {
    if (track[i].has_member("length")) {
      track_pieces_.push_back(
          std::unique_ptr<StraightTrackPiece>(new StraightTrackPiece {
            track[i]["length"].as_double()
          }));
    } else {
      track_pieces_.push_back(std::unique_ptr<BentTrackPiece>(new BentTrackPiece {
        track[i]["radius"].as_double(),
        track[i]["angle"].as_double()
      }));
    }
  }

  for (size_t i = 0; i < track_pieces_.size(); ++i) {
    std::cout << track_pieces_[i]->ToString() << std::endl;
  }

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  size_t i = 0;
  for (; i < data.size(); ++i) {
    if (data[i]["id"]["name"] == "iBrAaAazz")
      break;
  }

  const jsoncons::json& my_car = data[i];

  assert(i < data.size());

  int piece_index = my_car["piecePosition"]["pieceIndex"].as_int();
  double in_piece_distance = my_car["piecePosition"]["inPieceDistance"].as_double();
  double rem_distance = -1;
  if (!track_pieces_[piece_index]->is_bent()) {
    rem_distance =
        dynamic_cast<StraightTrackPiece*>(track_pieces_[piece_index].get())->length() - in_piece_distance;
  }

  double angle = GetMyAngle(my_car);
  std::cout << "Angle: " << angle << "\t";
  if (!track_pieces_[piece_index]->is_bent() &&
      track_pieces_[(piece_index + 1) % track_pieces_.size()]->is_bent() &&
      rem_distance <= 50) {
    std::cout << "In Piece Distance: " << in_piece_distance << "\t remaining distance = " << rem_distance << std::endl;
    return { make_throttle(0.3) };
  } else if (track_pieces_[piece_index]->is_bent()) {
    std::cout << "Throttle: " << std::min(0.6, 0.9 * (1.0 - angle / (2 + angle))) << std::endl;
    return { make_throttle(std::min(0.6, 0.9 * (1.0 - angle / (2 + angle)))) };
  }

  std::cout << "Throttle: " << 1.0 * (1.0 - angle / (2 + angle)) << std::endl;
  return { make_throttle(1.0 * (1.0 - angle / (2 + angle))) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

// static
double game_logic::GetMyAngle(const jsoncons::json& data) {
  double angle = data["angle"].as_double();
  return fabs(angle);
}
